function [ima,imb,bbox]= makeBBox(filenamea,filenameb,imgParams,varargin)
%%  this should show you the automatic bbox  and ask if its k
%TODO implement CV here
[ima,cma]= imread(filenamea);
[imb,cmb]= imread(filenameb);

ima=imresize(ima,imgParams.imgSize);
imb=imresize(imb,imgParams.imgSize);
h = ones(9,9) / 81;
tima=imfilter(ima,h);
timb=imfilter(imb,h);
h = ones(7,7) / 49;
tima=imfilter(tima,h);
timb=imfilter(timb,h);
h = ones(5,5) / 25;
tima=imfilter(tima,h);
timb=imfilter(timb,h);

dim=sum(abs(tima-timb),3);
dim=dim./max(dim(:));
level = graythresh(dim);
dimBool=dim>level ;

dimBool1=imdilate(dimBool,strel('square',3));
dimBool2=imerode(dimBool1,strel('square',5));
dimBool2=imdilate(dimBool2,strel('line',15,45));
dimBool2=imdilate(dimBool2,strel('line',15,-45));
dimBool2=imdilate(dimBool2,strel('diamond',3));
dimBool2=imdilate(dimBool2,strel('line',15,45));
dimBool2=imdilate(dimBool2,strel('line',15,-45));
dimBool3=imdilate(dimBool2,strel('square',3));
dimBool3=imdilate(dimBool3,strel('diamond',3));
dimBool3=imdilate(dimBool3,strel('square',11));

dimBool4=imerode(dimBool3,strel('square',11));
dimBool4=imerode(dimBool4,strel('disk',11));

final=dimBool4;
hblob = vision.BlobAnalysis;
hblob.AreaOutputPort = false;
hblob.BoundingBoxOutputPort = true;
hblob.MinimumBlobArea=300;
hblob.MaximumCount=3;
hblob.ExcludeBorderBlobs=true;

if isempty(varargin)
    f1=figure('Name','AutoBBox','units','normalized','outerposition',[0 0 1 1]);
else
    f1=varargin{1};
    set(f1,'Name','AutoBBox','units','normalized','outerposition',[0 0 1 1]);
end

try
    [CENTROID,BBOX] = step(hblob,logical(final));
    [~,idmax]=max(BBOX(:,3).*BBOX(:,4));
    bbox=BBOX(idmax,:);
    bbox=[(bbox(1)-bbox(3)*0.1) (bbox(2)-bbox(4)*0.1) bbox(3)*1 bbox(4)];
    compare = insertShape((ima./2+imb./2),'Rectangle',bbox,...
        'Color', 'red','LineWidth', 10);
    
catch
    disp('CV based BBox detection failed. Switching to manual');
    bbox=manualSelect(ima,imb,f1);
    return;
end

subplot(2,3,1)
imshow(dim);
title('img Difference')
subplot(2,3,2)
imshow(dimBool);
title('Bool >10%')
subplot(2,3,3)
imshow(dimBool1);
title('erode5')
subplot(2,3,4)
imshow(dimBool3);
title('dilute3')
subplot(2,3,5)
imshow(dimBool4);
title('dilute11')
subplot(2,3,6)
imshow(compare);
title('resultBBox')
%%
manual=myDiag();
if manual
    bbox=manualSelect(ima,imb,f1);
end
if isempty(varargin)
    close(f1);
else
    clf('reset');
end

end

function manual=myDiag
d = dialog('Position',[300 300 250 150],'Name','Is BBox ok?');
manual=logical(0);
btn1 = uicontrol('Parent',d,...
    'Position',[89 20 70 25],...
    'String','Auto is ok',...
    'Callback',@setA);

btn2 = uicontrol('Parent',d,...
    'Position',[89 45 70 25],...
    'String','Manual', 'Callback',@setM);
uiwait(d);

    function setM(a,b)
        manual=logical(1);   
        delete(gcf);
    end

 function setA(a,b)         
        delete(gcf);
    end


end

function bbox=manualSelect(ima,imb,varargin)
%% Manual:
if isempty(varargin)
    f=figure;
else
    f=varargin{1};
    set(f,'Name','AutoBBox','units','normalized','outerposition',[0 0 1 1]);
end
subplot(1,2,1)
h1=imshow(ima);
title('Select which image!');
subplot(1,2,2)
h2=imshow(imb);
title('Select which image!');
ginput(1);
title('Select Rectangle!!');
bbox=getrect;
if isempty(varargin)
    close(f);
else
    clf('reset');
end
end
