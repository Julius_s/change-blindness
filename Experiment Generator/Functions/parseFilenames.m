function UniqueNames = parseFilenames( filenames )
%  Takes in the a cell array of file names and asserts that all files are
%  pairs in the filename_a and filename_b sense. Also it disregards the
%  file endings and returns a cell array of unique filenames without the
%  endings _a and _b


if isnumeric(filenames)
    UniqueNames={};
    return;
end
files=cellfun(@(x)strsplit(x,'_'),filenames,'UniformOutput',logical(0));
% assert that all names appear in pairs
cellfun(@(x) assert(sum(cellfun(@(y)strcmp(x{1},y{1}),files))==2),files);
files=cellfun(@(x) x{1},files,'UniformOutput',logical(0));
UniqueNames={};
for ii=1:length(files)
    if ~any(cellfun(@(y)strcmp(files{ii},y),UniqueNames))
        UniqueNames{end+1}=files{ii};
    end    
end  

end

