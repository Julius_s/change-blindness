function paramStruct = genPerturbStruct( PertType,uniqueName,varargin )
paramStruct.PertType=PertType;
paramStruct.uniqueName=uniqueName;
switch PertType
    case 'Control'
%         Expected parameters in order:
%         ShowTimeA - amount of time pic A is shown ms
%         ShowTimeB - amount of time pic B is shown ms
%         TransTime - amount of time between images ms
%         ImageSize - size of the image
          paramStruct.ShowTimeA=varargin{1};
          paramStruct.ShowTimeB=varargin{2};                 
    case 'CInt' 
%         Expected parameters in order:
%         ShowTimeA - amount of time pic A is shown ms
%         ShowTimeB - amount of time pic B is shown ms
%         TransTime - amount of time between images ms
%         ImageSize - size of the image
%         Color - Color of the perturbation        
          paramStruct.ShowTimeA=varargin{1};
          paramStruct.ShowTimeB=varargin{2};
          paramStruct.TransTime=varargin{3};          
          paramStruct.Color=varargin{4};
          
    case 'CballD' 
%         Expected parameters in order:
%         ShowTimeA - amount of time pic A is shown ms
%         ShowTimeB - amount of time pic B is shown ms
%         TransTime - amount of time between images ms
%         ImageSize - size of the image
%         Color - Color of the perturbation
%         Area - % Area of the image taken
%         Number - Number of balls
          paramStruct.ShowTimeA=varargin{1};
          paramStruct.ShowTimeB=varargin{2};
          paramStruct.TransTime=varargin{3};          
          paramStruct.Color=varargin{4};
          paramStruct.Area=varargin{5};
          paramStruct.Number=varargin{6};
end

