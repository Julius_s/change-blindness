function [ FileNames ] = selecttAllFiles
% Creates a structure FileNames which has the following fields:
%     blank - image for image between Animations
%     thanks - Image shown after final Animation
%     examples - Animations that will form the example sequence
%     Animations - Animations that will  make the main sequence

clear FileNames
%% Select image file for blank screen screen
[FileNames.blank,PathName,~] = uigetfile('.jpg','MultiSelect','off','Select image file for blank screen screen');
addpath(PathName);
disp(strcat(PathName,'    <-- Has been added to the path'))
%% Select image file for thank you screen
[FileNames.thanks,PathName,~] = uigetfile('.jpg','MultiSelect','off','Select image file for thank you screen');
addpath(PathName);
disp(strcat(PathName,'    <-- Has been added to the path'))
%% Select image files for example animations
[FileNames.examples,PathName,~] = uigetfile('.jpg','MultiSelect','on','Select image files for example animations');
if PathName~=0
    addpath(PathName);
    disp(strcat(PathName,'    <-- Has been added to the path'))
else
    disp('No examples selected')
    
end
%% Select image files for the main Animations
[FileNames.Animations,PathName,~] = uigetfile('.jpg','MultiSelect','on','Select image files for the main Animations');
addpath(PathName);
disp(strcat(PathName,'    <-- Has been added to the path'))
end

