function [trans1,trans2,showTime]=genPert( ima,imb,pertParam,bbox,varargin)

switch pertParam.PertType
    case 'Control'
        %         Expected parameters in order:
        %         ShowTimeA - amount of time pic A is shown ms
        %         ShowTimeB - amount of time pic B is shown ms
        
        showTime=[pertParam.ShowTimeA pertParam.ShowTimeB]';
        trans1=[];
        trans2=[];
    case 'CInt'
        %         Expected parameters in order:
        %         ShowTimeA - amount of time pic A is shown ms
        %         ShowTimeB - amount of time pic B is shown ms
        %         TransTime - amount of time between images ms
        %         Color - Color of the perturbation
        
        showTime=[...
            pertParam.ShowTimeA;...
            pertParam.TransTime;...
            pertParam.ShowTimeB;...
            pertParam.TransTime];
        switch pertParam.Color
            case 'grey'
                greyIm=ones(size(ima))*128;
                trans1=greyIm;
                trans2=greyIm;
            case 'inv'
                inv1=255-ima;
                inv2=255-imb;
                trans1=inv1;
                trans2=inv2;
        end
        
    case 'CballD'
        %         Expected parameters in order:
        %         ShowTimeA - amount of time pic A is shown ms
        %         ShowTimeB - amount of time pic B is shown ms
        %         TransTime - amount of time between images ms
        %         Color - Color of the perturbation
        %         Area - % Area of the image taken
        %         Number - Number of balls
        
        %TODO make this less awkward
        shapeInserter = vision.ShapeInserter('Shape','Circles', ...
            'Fill',1,'Opacity',1);
        shapeInserter.FillColor='Custom';
        imgSize=size(ima(:,:,1));
        
        showTime=[...
            pertParam.ShowTimeA;...
            pertParam.TransTime;...
            pertParam.ShowTimeB;...
            pertParam.TransTime];
        
        switch pertParam.Color
            case 'grey'
                color=ones(1,3)*128;
            case 'green'
                color=[0 200 0];
            case 'pink'
                color=[200 50 50];
            case 'white'
                color=ones(1,3)*250;
        end
        
        shapeInserter.CustomFillColor=int32(color);
        bbox=round(bbox);
        if isempty(varargin)
            f=figure;
        else
            f=varargin{1};
            set(f,'Name','perTurb');
        end
        
        
        radius=round(sqrt(((pertParam.Area*imgSize(1)*imgSize(2))/pertParam.Number)/pi));
        
        while 1
            tempIm=ima;
            %insert balls
            [ allCoo, flag ] = randomBallIInserter( imgSize,radius+1,pertParam.Number,bbox );
            
            if flag==-1; disp('insert failed'); end
            
            for ii=1:pertParam.Number
                tempIm=step(shapeInserter, tempIm, int32([allCoo(ii,1),allCoo(ii,2),radius]));
            end
            imshow(tempIm);
            s=input('Satisfied?','s');
            if isempty(s)
                break
            end
        end
        trans1=tempIm;
        
        
        while 1
            tempIm=imb;
            %insert balls
            [ allCoo, flag ] = randomBallIInserter( imgSize,radius+1,pertParam.Number,bbox );
            
            if flag==-1; disp('insert failed'); end
            
            for ii=1:pertParam.Number
                tempIm=step(shapeInserter, tempIm, int32([allCoo(ii,1),allCoo(ii,2),radius]));
            end
            imshow(tempIm);
            s=input('Satisfied?','s');
            if isempty(s)
                break
            end
        end
        trans2=tempIm;
        if isempty(varargin)
            close(f);
        else
%             clf('reset');
        end
end

