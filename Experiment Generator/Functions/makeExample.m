function exampleSeq=makeExample(uniqueNames,imgParams)

pertParam=genPerturbStruct( 'Control','',1000,1000);
exampleSeq=cell(1,length(uniqueNames));
f=figure;
for jj=1:length(uniqueNames)
    
    [ima,imb,bbox]= makeBBox(...
        strcat(uniqueNames{jj},'_a.jpg'),...
        strcat(uniqueNames{jj},'_b.jpg'),...
        imgParams,f);
    
    [trans1,trans2,showTime]=genPert(ima,imb,pertParam,f);
    
    anim.pert=cat(4,ima,trans1,imb,trans2);
    anim.st=showTime;
    anim.name=sprintf('ex %d',jj);
    anim.bbox=bbox;
    
    exampleSeq{1,jj}=anim;
end
close(f);