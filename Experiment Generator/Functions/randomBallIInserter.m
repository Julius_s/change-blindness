function [ allCoo, flag ] = randomBallIInserter( sz,radius,n,bbox )
flag=1;
allCoo=zeros(n,2);
boolSI = vision.ShapeInserter('Shape','Circles', ...
    'Fill',1,'Opacity',1);
boolSI.FillColor='Custom';
boolSI.CustomFillColor=int32(0);

se =strel('disk', radius, 8);

originalBW=true(sz(1),sz(2));
originalBW(1,:)=false;
originalBW(end,:)=false;
originalBW(:,1)=false;
originalBW(:,end)=false;
% assume bbox=[xmin,ymin, xlength,ylength]
bbox=round(bbox);
originalBW(bbox(2):bbox(2)+bbox(4),bbox(1):bbox(1)+bbox(3))=false;

erodedBW = imerode(originalBW,se);

for ii=1:n
    allNonZero=find(erodedBW);    
    if isempty(allNonZero)
        flag=-1;
        break
    end
    point=allNonZero(randi(length(allNonZero)));
    [x,y] = ind2sub(size(erodedBW),point);
    allCoo(ii,:)=[y,x];
    originalBW=logical(step(boolSI, originalBW, int32([y,x,radius])));
    erodedBW = imerode(originalBW,se);
%     subplot(1,2,1)
%     imshow(originalBW)
%      subplot(1,2,2)
%     imshow(erodedBW)
end

end

