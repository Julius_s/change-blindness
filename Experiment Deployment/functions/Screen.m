function varargout = Screen(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @Screen_OpeningFcn, ...
    'gui_OutputFcn',  @Screen_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
end

function Screen_OpeningFcn(hObject, eventdata, handles, varargin)
image_size=varargin{1};
handles.showTime=[1 1];
handles.image_size=image_size;
handles.im= zeros([image_size(2) image_size(1)]);
handles.display = imshow(handles.im);%loads the first image along with its colormap
handles.guifig = gcf;
% guidata(handles.guifig, handles);
axesHandle  = get(handles.display,'Parent');
axesHandle.Units='pixels';
set(handles.display,'ButtonDownFcn',{@image_ButtonDownFcn,handles});
handles.axesHandle  = handle(axesHandle);
handles.count = 1;% intialise counter to update the next frame
handles.tmr = timer('TimerFcn', {@TmrFcn,handles.guifig},'BusyMode','Queue',...
    'ExecutionMode','FixedRate','Period',handles.showTime(1)/1000); %form a Timer Object
set(handles.display,'ButtonDownFcn',{@image_ButtonDownFcn,handles});
handles.useEyeTrack=0;

handles.output = hObject;

guidata(hObject, handles);
guidata(handles.guifig, handles);
end

function varargout = Screen_OutputFcn(hObject, eventdata, handles)
warning('off','MATLAB:HandleGraphics:ObsoletedProperty:JavaFrame');
hFig=hObject;
hFig.Units='pixels';
hFig.Resize='off';
set(hFig,'menubar','none');
set(hFig,'NumberTitle','off');
drawnow;
% Get the underlying Java JFrame reference handle
mjf = get(handle(hFig), 'JavaFrame');
jWindow = mjf.fHG2Client.getWindow;
% get borders
% jWindow.setAlwaysOnTop(true)
jBorders=jWindow.getInsets();
borders=[jBorders.top jBorders.bottom jBorders.left jBorders.right];
%  get monitor resolutuion
set(0,'units','pixels') ;
Pix_SS =get(0,'MonitorPositions');
%using java to get screen size - more reliable. For window size.  
javaDim=java.awt.Toolkit.getDefaultToolkit().getScreenSize();      
Pix_SS=Pix_SS(1,:);
handles.Pix_SS=Pix_SS;
% set figure size and location
jWindow.setSize(javaDim.width+borders(3)+borders(4), javaDim.height+2*(borders(1)+borders(2)));
jWindow.setLocation(-borders(3),-borders(1));
% center the image
handles.axesHandle.Units='pixels';
handles.padding=[(Pix_SS(3)-handles.image_size(1))/2 ...
    (Pix_SS(4)-handles.image_size(2))/2];
% position the panel. Noet hardcoded because of the panel size
handles.InfoPanel.Units='pixels';
handles.InfoPanel.Position=[(Pix_SS(3)-950)/2 ...
    (Pix_SS(4)-650)/2  [950 650] ];

set(handles.axesHandle,'Position',[handles.padding handles.image_size ]);
drawnow;
% a=handles.InfoPanel.Children(end);
% imshow('Splash.png','Parent',a);
% a.Position=[25 25 900 600];
handles.JframeRef=jWindow;
% handles.JframeRef.setAlwaysOnTop(true);
guidata(hObject,handles);
varargout{1} = handles.output;
end



function AgeMenu_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

function StartB_Callback(hObject, eventdata, handles)
handles.InfoPanel.Visible='off';
set(handles.display,'visible','on')
uiresume(handles.output);
end



function TmrFcn(src,event,handles)
%Timer Function to animate the GIF
handles = guidata(handles);
set(handles.display,'CData',handles.im(:,:,:,handles.count)); %update the frame in the axis
stop(handles.tmr);
set(handles.tmr,'Period',handles.showTime( handles.count)/1000);%redundant but set just for uniformity
set(handles.tmr,'StartDelay',handles.showTime( handles.count)/1000);
handles.count = handles.count + 1; %increment to next frame
if handles.count > handles.len %if the last frame is achieved intialise to first frame
    handles.count = 1;
end
guidata(handles.figure1, handles);
start(handles.tmr);
end

function image_ButtonDownFcn(hObject, eventdata,handles)
allHandles=guidata(handles.figure1);
altcoordinates=eventdata.IntersectionPoint(1:2);
% this is y from top to bottom 

% This would be y from bottom to top
% altcoordinates(2)=allHandles.image_size(2)-altcoordinates(2);
allHandles.coordinates=round(altcoordinates);
guidata(allHandles.figure1,allHandles);
uiresume(allHandles.figure1);
end

% --- Executes on button press in exitButton.
function exitButton_Callback(hObject, eventdata, handles)
uiresume(handles.output);
handles.JframeRef.dispose();
myex('d');
close(handles.output);
end

% --- Executes on button press in eyeTrackCheck.
function eyeTrackCheck_Callback(hObject, eventdata, handles)
handles.useEyeTrack=get(hObject,'Value') ;
guidata(handles.output,handles);
end

% --- Executes on button press in CalibButton.
function CalibButton_Callback(hObject, eventdata, handles)

if strcmp(hObject.String,'calibrate')
    
    try
        myex('q');
    catch
        warning('calibration to eyetrack failed');
        %todo user visible warning
    end
    
else
    set(findall(handles.InfoPanel, '-property', 'enable'), 'enable', 'off');
    handles.eyeTrackCheck.Enable='off';
    drawnow;
    try
        myex('c');
        set(findall(handles.InfoPanel, '-property', 'enable'), 'enable', 'on');
        handles.eyeTrackCheck.Enable='off';
    catch
        warning('connecting to eyetrack failed');
        %todo user visible warning
    end
    handles.eyeTrackCheck.Enable='on';
    hObject.String='calibrate';
    
end

end


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
try
stop(handles.tmr);
catch
end
delete(hObject);
end
