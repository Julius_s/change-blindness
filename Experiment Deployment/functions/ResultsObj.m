classdef ResultsObj<handle
    properties
        % structure array of result structures
        results       
    end
    
    methods
        function storeResult(obj,PlayerOptStruct,seqDataStruct,exampleDataStruct,seed)
            result.PlayerOptions=PlayerOptStruct;
            result.SequenceData=seqDataStruct;
            result.ExampleData=exampleDataStruct;
            result.seed=seed;
            obj.results{end+1}=result;
        end
        
        function writeAllToCsv(obj,filename)
            while exist(filename)
                 filename=['a' '_' filename];
            end                       
            fid=fopen(filename ,'w');
            for ii=1:length(obj.results)
                writeSingleResult(fid,obj.results{ii},ii);
            end
            fclose(fid);
        end
        
        function appendWithLast(obj,filename)
            if ~exist(filename)
                disp('appendFile was not found')
            end
             fid=fopen(filename ,'a');
             writeSingleResult(fid,obj.results{end},length(obj.results));
             fclose(fid);
        end
        
        function saveTo(obj,name)
            save(name,'obj');
        end
    end
    methods (Static)
        function dataStruct =genDataStruct(names,responseTimes,hit,coo,gaze)
            dataStruct.names=names(:);
            dataStruct.responseTimes=responseTimes(:);
            dataStruct.hit=hit(:);
            dataStruct.coo=coo';
            dataStruct.gaze=gaze;
        end
        function playerOptStruct =genPlayerOptStruct(genderString,conditionString,...
                ageString)
            playerOptStruct.time=strrep(strrep(strrep(datestr(now),':','_'),' ','_'),'-','_');%to string well
            playerOptStruct.gender=genderString;
            playerOptStruct.age=[ageString{:}];
            playerOptStruct.condition=conditionString;
        end
    end
end

function writeSingleResult(fid,result,resultNum)
fprintf(fid,'Result, %d \n',resultNum);
fprintf(fid,'%s,%s,%s,%s\n',...
    result.PlayerOptions.time,...
    result.PlayerOptions.gender,...
    result.PlayerOptions.age,...
    result.PlayerOptions.condition...
    );
fprintf(fid,'Example Info:\n');
writeseq(fid, result.ExampleData);
fprintf(fid,'Sequence seed %d Info:\n',result.seed);
writeseq(fid, result.SequenceData);
fprintf(fid,'\n');
end

function writeseq(fid,seq)
strString=[repmat('%s,',1,length(seq.names)-1) '%s\n'];
doubleString=[repmat('%g,',1,length(seq.names)-1) '%g\n'];
intString=[repmat('%d,',1,length(seq.names)-1) '%d\n'];

fprintf(fid,'Perturbation Names,');
fprintf(fid,strString,seq.names{:});

fprintf(fid,'Response Times,');
temp=mat2cell(seq.responseTimes,ones(1,length(seq.responseTimes)),1);
fprintf(fid,doubleString,temp{:});

fprintf(fid,'Hit,');
temp=mat2cell(seq.hit,ones(1,length(seq.hit)),1);
fprintf(fid,intString,temp{:});

fprintf(fid,'X,');
temp=mat2cell(seq.coo(:,1),ones(1,length(seq.coo(:,1))),1);
fprintf(fid,intString,temp{:});

fprintf(fid,'Y,');
temp=mat2cell(seq.coo(:,2),ones(1,length(seq.coo(:,2))),1);
fprintf(fid,intString,temp{:});

end
