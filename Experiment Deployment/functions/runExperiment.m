function  [exitFlag, saveFlag] = runExperiment(seq,seed,exampleSeq,resultsObject,blank,thanks,varargin)
%Expected Animation structure:
% pert-4Duint8
% st=showtimes milisecods
% name=name
% bbox x,y,xlength,ylength
saveFlag=0;
exitFlag=0;
if isempty(varargin)
    myScreen=Screen([800 600]);
else
    myScreen=varargin{1};
end
handles=guidata(myScreen);
%Waiting for start of experiment;
uiwait(handles.figure1);
if ~myScreen.isvalid
    exitFlag=1;
    return
end
handles=guidata(myScreen);


%collect player info:
[sex,vision,age] = playerInfoParser(handles);
playerData=resultsObject.genPlayerOptStruct(sex,vision,age);

%tabo for now
% if PlayerData.eyeTrackingEnable % need to check that data is legit
%     temp=myex('g');
%     pause(2);
%     PlayerData.eyeTimeSync(1,2)=temp(end,3);
% end

%% run Examples
exampleDataStruct=runAnimSeq(exampleSeq);
%% run sequences
seqDataStruct=runAnimSeq(seq(randperm(length(seq))));
%% Thank you
showIm(myScreen,thanks);
pause(0.5);
uiwait(handles.figure1);
showIm(myScreen,thanks);
uiwait(handles.figure1);
 handles=guidata(myScreen);
if checkHit(thanks.bbox,handles.coordinates(:))
    saveFlag=1;
    resultsObject.storeResult(playerData,seqDataStruct,exampleDataStruct,seed)
end
if ~myScreen.isvalid
    exitFlag=1;
    return
end
handles=guidata(myScreen);
stop(handles.tmr);
%Reset the experiment.
set(handles.display,'visible','off');
handles.InfoPanel.Visible='on';
if ~myScreen.isvalid
    exitFlag=1;
    return
end

%%--------
    function resultDataStruct=runAnimSeq(animSeq)
        nn=length(animSeq);
        names=cell(1,nn);
        times=-ones(1,nn);
        hit=false(1,nn);
        coo=-ones(2,nn);
        gaze=[];
        for jj=1:length(animSeq)
             % pause
            showIm(myScreen,blank);
            uiwait(handles.figure1);
            if ~myScreen.isvalid
                exitFlag=1;
                return
            end
            
            names{jj}=animSeq{jj}.name;
            showIm(myScreen,animSeq{jj});
            handles=guidata(myScreen);
            
            tic;
            uiwait(handles.figure1);
            times(jj)=toc;
            if ~myScreen.isvalid
                exitFlag=1;
                return
            end
            handles=guidata(myScreen);
            
            coo(:,jj)=handles.coordinates(:);
            hit(jj)=checkHit(animSeq{jj}.bbox,coo(:,jj));
           
        end
        resultDataStruct =resultsObject.genDataStruct(names,times,hit,coo,gaze);
    end
end

function hit=checkHit(bbox,coo)
hit=false;
if coo(1)>=bbox(1) &&coo(1)<=bbox(1)+bbox(3)
    if coo(2)>=bbox(2) &&coo(2)<=bbox(2)+bbox(4)
        hit=true;
    end
end
end


% clear all % clear all still crashes matlab because of eye gaze thing

% timestamp is in mico seconds(but what is the data type? double or int?)
% todo write a handle clearing thing for eye gaze
% todo should there be an alternative to not send results?


% %% save data
% fn=strcat('results\',strrep(strrep(strrep(datestr(now),':','_'),' ','_'),'-','_'),num2str(round(rand(1)*100)));
% % if PlayerData.eyeTrackingEnable % need to check that data is legit
% %     temp=myex('g');
% %     pause(5);
% %     save(strcat(fn,'_eyes.mat'),'temp');
% %     myex('d');
% % end
% save(strcat(fn,'.mat'),'PlayerData');
%%
