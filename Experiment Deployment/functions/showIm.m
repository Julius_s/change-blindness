function  showIm( hFig , currentAnim)
handles=guidata(hFig);
handles.im=currentAnim.pert;
handles.showTime=currentAnim.st;
handles.len = size(handles.im,4); % number of frames in the gif image imshow(yourImage,);
handles.count=1;
stop(handles.tmr);
set(handles.display,'CData',handles.im(:,:,:,handles.count));
set(handles.tmr,'Period',handles.showTime(1));
set(handles.tmr,'StartDelay',0);
guidata(handles.guifig, handles);
guidata(handles.output, handles);
start(handles.tmr);
end

