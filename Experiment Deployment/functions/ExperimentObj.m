classdef ExperimentObj<handle
    %Concepts:
    %perturbation:
    %a distortion of an image. animatiion =perturbation(number,image)
    
    %Animation:
    %full structure to display  4 images with showtimes
    %image
    properties %(Access=private)
        expName; %experiment name
        Images;  % Mx2 cell array;
        Perturbations; %3D cell array {m-image index,n-perturbation index,version 1-2}
        ShowTimes;    %         2D cell array
        BBoxes; %bbox is [xmin,ymin,xlength,ylength]
        UniqueNames;% MxN array with unique perturbation names
        N;%perturbations
        M;% images
        exampleSeq; % example sequence
        blank; % blank animation
        thanks;% thanks animation
    end
    
    methods
        function obj=ExperimentObj(FileNames,PertParams,ImageParams,ExpName)
            %Assign parameters
            N=length(PertParams);
            M=length(FileNames.UniqueNames);
            obj.M=M;
            obj.N=N;
            obj.expName=ExpName;
            %% Start Main sequence generation
            %preallocation
            
            obj.Images=cell(M,2);
            obj.BBoxes=zeros(M,4);
            obj.Perturbations=cell(N,M,2);
            obj.UniqueNames=cell(N,M);
            obj.ShowTimes=cell(N,M);
            f=figure;
            for jj=1:M
                [obj.Images{jj,1},obj.Images{jj,2},obj.BBoxes(jj,:)]= makeBBox(...
                    strcat(FileNames.UniqueNames{jj},'_a.jpg'),...
                    strcat(FileNames.UniqueNames{jj},'_b.jpg'),...
                    ImageParams,f);
            end
            for jj=1:M
                for ii=1:N
                    %generate unique names
                    obj.UniqueNames{ii,jj}=strcat(FileNames.UniqueNames{jj},...
                        '_',PertParams{ii}.uniqueName);
                    %generate perturbations
                    [obj.Perturbations{ii,jj,1},obj.Perturbations{ii,jj,2},...
                        obj.ShowTimes{ii,jj}]=genPert(obj.Images{jj,1},...
                        obj.Images{jj,2},PertParams{ii},obj.BBoxes(jj,:),f);
                end
            end
            close(f);
            
            %% Start example sequence generation
            obj.exampleSeq=makeExample(FileNames.exUniqNames,ImageParams);
            %% Assign thank
            blank.pert=imread(FileNames.blank);
            blank.st=1000;
            blank.name='blank';
            blank.bbox=ones(1,4);
            thanks.pert=imread(FileNames.thanks);
            thanks.st=1000;
            thanks.name='thanks';
            thanks.bbox=[30 350 300 275];
%             thanks.bbox=ones(1,4);
            obj.blank=blank;
            obj.thanks=thanks;
        end
        %returns a 4D numeric array (rows,colmuns,rgb,1-4)
        function pert=getPert(obj,n,m)
            pert=cat(4,obj.Images{m,1},...
                obj.Perturbations{n,m,1},...
                obj.Images{m,2},...
                obj.Perturbations{n,m,2});
        end
        %returns 1x4 numeric array in miliseconds
        function st=getShowTime(obj,n,m)
            st=obj.ShowTimes{n,m};
        end
        %         returns concatinated image and perturbation name
        function name=getName(obj,n,m)
            name=   obj.UniqueNames{n,m};
        end
        
        function bbox=getBBox(obj,m)
            bbox=round(obj.BBoxes(m,:));
        end
        %         returns anim structure with 4 images and showtimes and bbox and
        %         imagename and perturbation name
        function anim=getAnim(obj,n,m)
            anim.pert=getPert(obj,n,m);
            anim.st=getShowTime(obj,n,m);
            anim.name=getName(obj,n,m);
            anim.bbox=getBBox(obj,m);
        end
        %         returns a full sequence for a single testimonie to observe.
        function seq=getSeq(obj,seed)
            M=obj.M;
            N=obj.N;
            %seed is a shift number in this context
            seed=seed-1;
            id=(1+mod((seed:seed+M-1),N))+(0:N:N*(M-1));
            seq=cell(M,1);
            temp=zeros(N,M);
            temp(id)=1;
            [n m]=find(temp);
            for ii=1:length(id)
                seq{ii}= getAnim(obj,n(ii),m(ii));
            end
        end
        
        function saveMainSeq(obj)
            M=obj.M;
            N=obj.N;
            %this function should save main sequance
            dirname=strcat(obj.expName,'_main');
            if isdir(dirname)
                rmdir(dirname)
            end
            mkdir(dirname);
            for ii=1:M
                %get image name
                imName=strsplit(obj.UniqueNames{1,ii},'_');
                imName=imName{1};
                %                 Create dir for image
                mkdir([dirname '/' imName]);
                %write images
                imwrite(obj.Images{ii,1},[dirname '/' imName '/' imName '_a.jpg']);
                imwrite(obj.Images{ii,2},[dirname '/' imName '/' imName '_b.jpg']);
                %write perts
                for jj=1:N
                    if ~isempty(obj.Perturbations{jj,ii,1})
                        imwrite(obj.Perturbations{jj,ii,1},[dirname '/' imName '/' obj.UniqueNames{jj,ii} '_a.jpg']);
                    end
                    if ~isempty(obj.Perturbations{jj,ii,2})
                        imwrite(obj.Perturbations{jj,ii,2},[dirname '/' imName '/' obj.UniqueNames{jj,ii} '_b.jpg']);
                    end
                end
            end
            obj.Images={};
            obj.Perturbations={};
            Experiment=obj;
            save([dirname '\Experiment'],'Experiment');
            zip(dirname,dirname);
            rmdir(dirname,'s');
        end
        
        function loadMainSeq(obj,fullPath)
            dirname=strsplit(fullPath,'\');
            dirname=dirname{end};
            %%
            M=obj.M;
            N=obj.N;
            obj.Images=cell(M,2);
            obj.Perturbations=cell(N,M,2);
            %%
            for ii=1:M
                %get image name
                imName=strsplit(obj.UniqueNames{1,ii},'_');
                imName=imName{1};
                % Read images
                obj.Images{ii,1}=imread([dirname '/' imName '/' imName '_a.jpg']);
                obj.Images{ii,2}=imread([dirname '/' imName '/' imName '_b.jpg']);
                for jj=1:N
                    name=[dirname '/' imName '/' obj.UniqueNames{jj,ii}];
                    if exist([name '_a.jpg'],'file')
                        obj.Perturbations{jj,ii,1}=imread([name '_a.jpg']);
                    end
                    if exist([name '_b.jpg'],'file')
                        obj.Perturbations{jj,ii,2}=imread([name '_b.jpg']);
                    end
                end
            end
            rmdir(dirname,'s');
        end
        
        function isEqual=compare(obj,obj2)
            isEqual=0;
            if ~(obj.M==obj2.M && ...
                    obj.N==obj2.N && ...
                    strcmp(obj.expName,obj2.expName) && ...
                    all(obj.BBoxes(:)==obj2.BBoxes(:)))
                disp('params failed')
                return
            end
            for ii=1:obj.M
                im1= obj.Images{ii};
                im2= obj2.Images{ii};
                if ~all(im1(:)==im2(:))
                    disp('images failed')
                    return
                end
            end
            for ii=1:obj.M *obj.N
                im1= obj.Perturbations{ii};
                im2= obj2.Perturbations{ii};
                if ~all(im1(:)==im2(:))
                    disp('perts failed')
                    return
                end
            end
            for ii=1:numel(obj.ShowTimes)
                im1= obj.ShowTimes{ii};
                im2= obj2.ShowTimes{ii};
                if ~all(im1(:)==im2(:))
                    disp('showtimes failed')
                    return
                end
            end
            for ii=1:obj.M *obj.N
                im1= obj.UniqueNames{ii};
                im2= obj2.UniqueNames{ii};
                if ~strcmp(im1,im2)
                    disp('unique names failed')
                    return
                end
            end
            isEqual=1;
        end
    end
end

