function [sexString,visionString,ageString] = playerInfoParser(handles)
sexString=strtrim(handles.sexGroup.SelectedObject.String);%Male/Female/NotGiven
visionString=strtrim(strrep(handles.visionGroup.SelectedObject.String, '' ,'_'));%n/c/i
ageString=strtrim(strrep(handles.AgeMenu.String(handles.AgeMenu.Value), '-' ,'_'));%n/0/10/20/30/40/50/60/70/80/90
%Reset values
handles.sexGroup.SelectedObject=handles.sexGroup.Children(1);
handles.visionGroup.SelectedObject=handles.visionGroup.Children(1);
handles.AgeMenu.Value=1;
end