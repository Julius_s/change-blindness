function [ PlayerInfo ] = genPlayerInfo(type)
switch type
    case 'empty'
        PlayerInfo=genEmpty();
    otherwise
        PlayerInfo=genDefault();
end

end
function  PlayerInfo  = genEmpty()
PlayerInfo.sex='x';%m/f
PlayerInfo.vision='x';%n/c/i
PlayerInfo.age='x';%n/0/10/20/30/40/50/60/70/80/90
end

function  PlayerInfo  = genDefault()
PlayerInfo.sex='m';%m/f
PlayerInfo.vision='n';%n/c/i
PlayerInfo.age='n';%n/0/10/20/30/40/50/60/70/80/90
end

