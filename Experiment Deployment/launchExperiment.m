function launchExperiment
%% Select experiment file
[expFileName,PathName,~] = uigetfile('.zip','MultiSelect','off','Select experiment zip to set up')
addpath(PathName);
unzip([PathName  expFileName]);
% addpath('resources')
% addpath('functions')
load([expFileName(1:end-4) '\Experiment.mat' ]);
%% Load experiment
Experiment.loadMainSeq(expFileName(1:end-4));
%%
exitFlag=0;

ii= inputdlg(sprintf('Experiment with %d Images and %d perturbations loaded. Enter number 1 to %d for seed',Experiment.M,Experiment.N,Experiment.N),...
    'Change Blindness Experiment', 1);
ii=str2num([ii{:}]);
myScreen=Screen([800 600]);
resultsObj=ResultsObj;
exampleSeq=Experiment.exampleSeq;
%% Main Loop
nameBase=['StartOfSessionOn_' makeDateStr() ];
appendingFilename=([nameBase '.csv']);
fid=fopen(appendingFilename,'w');
fclose(fid);

while ~exitFlag
    [exitFlag saveFlag] = runExperiment(Experiment.getSeq(ii),ii,exampleSeq,resultsObj,...
        Experiment.blank,Experiment.thanks,myScreen);
    
    if saveFlag
        save([nameBase '.mat'],'resultsObj');
        resultsObj.appendWithLast(appendingFilename);
    end
    
    if exitFlag
        msgbox(sprintf('Last next time enter seed %d',ii));
        break
    end
    
    if saveFlag
        ii=ii+1;
    end
end
if myScreen.isvalid
    close(myScreen);
end
fn=['EndOfSession_' makeDateStr() ];
save([fn '.mat'],'resultsObj');
resultsObj.writeAllToCsv([fn '.csv']);

end
function dStr=makeDateStr()
dStr=strrep(strrep(strrep(datestr(now),':','_'),' ','_'),'-','_');
end

